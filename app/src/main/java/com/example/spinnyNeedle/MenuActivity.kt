package com.example.spinnyNeedle

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity

class MenuActivity : AppCompatActivity() {
    private lateinit var buttonCompass: Button
    private lateinit var buttonAccelerometer: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        buttonCompass = findViewById(R.id.menu_button_compass)
        buttonAccelerometer = findViewById(R.id.menu_button_accelerometer)

        buttonCompass.setOnClickListener {
            startActivity(Intent(this, CompassActivity::class.java))
        }

        buttonAccelerometer.setOnClickListener {
            startActivity(Intent(this, AccelerometerActivity::class.java))
        }
    }
}