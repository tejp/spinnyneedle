package com.example.spinnyNeedle

import android.content.Context
import android.content.Intent
import android.graphics.Color.argb
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

class AccelerometerActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var accelerometerValuesText: TextView
    private lateinit var menuButton: Button
    private lateinit var background: View
    private lateinit var sensorManager: SensorManager
    private var sensorAccelerometer: Sensor? = null

    private val gravity = floatArrayOf(0f, 0f, 0f)
    private var color = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accelerometer)

        accelerometerValuesText = findViewById(R.id.accelerometer_values_text)
        menuButton = findViewById(R.id.menu_button)
        background = findViewById(R.id.accelerometer_background)

        menuButton.setOnClickListener {
            startActivity(Intent(this, MenuActivity::class.java))
        }

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)

        when (sensorAccelerometer) {
            is Sensor -> sensorManager.registerListener(
                this,
                sensorAccelerometer,
                SensorManager.SENSOR_DELAY_GAME
            )
            else -> accelerometerValuesText.text = "Sensor not available!"
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}

    override fun onSensorChanged(event: SensorEvent?) {
        when (event?.sensor?.type) {
            Sensor.TYPE_ACCELEROMETER -> {

                val alpha = 0.8f

                gravity[0] = alpha * gravity[0] + (1 - alpha) * event.values[0]
                gravity[1] = alpha * gravity[1] + (1 - alpha) * event.values[1]
                gravity[2] = alpha * gravity[2] + (1 - alpha) * event.values[2]

                val (x, y, z) = Triple(
                    event.values[0] - gravity[0],
                    event.values[1] - gravity[1],
                    event.values[2] - gravity[2])

                val acc = sqrt(x.pow(2) + y.pow(2) + z.pow(2)).roundToInt()
                color = if (acc > color) acc else color

                val a = if (color > 50) 255 else color * 4
                val r = if (color > 45) 255 else 0
                val g = 255 - r
                val b = 0

                background.setBackgroundColor(argb(a, r, g, b))

                accelerometerValuesText.text = """
                    x: ${x.roundToInt()}
                    y: ${y.roundToInt()}
                    z: ${z.roundToInt()}
                    record: $color
                """.trimIndent()
            }
        }
    }

    override fun onResume() {
        super.onResume()

        sensorAccelerometer?.also {
            sensorManager.registerListener(this, it, SensorManager.SENSOR_DELAY_GAME)
        }
    }

    override fun onPause() {
        super.onPause()

        sensorManager.unregisterListener(this)
    }
}
