package com.example.spinnyNeedle

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlin.math.PI
import kotlin.math.absoluteValue
import kotlin.math.roundToInt


class CompassActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var compassText: TextView
    private lateinit var compassImage: ImageView
    private lateinit var menuButton: Button
    private lateinit var sensorManager: SensorManager
    private lateinit var sensors: List<Sensor>

    private val lastAccelerometer = FloatArray(3)
    private val lastMagnetometer = FloatArray(3)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compass)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        initSensors()

        compassText = findViewById(R.id.compass_text)
        compassImage = findViewById(R.id.compass_image)
        menuButton = findViewById(R.id.menu_button)

        menuButton.setOnClickListener {
            startActivity(Intent(this, MenuActivity::class.java))
        }
    }

    private fun initSensors() {

        val sensorRotationalVector =
            sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
                ?.let { listOf(it) }

        val sensorsMAMF = {
            listOf(Sensor.TYPE_ACCELEROMETER, Sensor.TYPE_MAGNETIC_FIELD)
                .mapNotNull { sensorManager.getDefaultSensor(it) }
                .takeIf { it.size == 2 }
        }

        sensors = sensorRotationalVector ?: sensorsMAMF() ?: emptyList()

        if (sensors.isEmpty())
            compassText.text = "Sensors are not available!"

        sensors.forEach {
            sensorManager.registerListener(this, it, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onSensorChanged(event: SensorEvent?) {

        var mRotation: FloatArray? = FloatArray(9)

        when (event?.sensor?.type) {
            Sensor.TYPE_ROTATION_VECTOR ->
                SensorManager.getRotationMatrixFromVector(mRotation, event.values)
            Sensor.TYPE_ACCELEROMETER -> {
                System.arraycopy(event.values, 0, lastAccelerometer, 0, event.values.size)
                SensorManager.getRotationMatrix(
                    mRotation,
                    null,
                    lastAccelerometer,
                    lastMagnetometer
                )
            }
            Sensor.TYPE_MAGNETIC_FIELD -> {
                System.arraycopy(event.values, 0, lastMagnetometer, 0, event.values.size)
                SensorManager.getRotationMatrix(
                    mRotation,
                    null,
                    lastAccelerometer,
                    lastMagnetometer
                )
            }
            else -> mRotation = null
        }

        mRotation?.let {
            val orientation = SensorManager.getOrientation(it, FloatArray(3))
            val azimuth = Math.toDegrees(orientation[0] + 2 * PI) % 360
            val roll = orientation[2]

            compassImage.rotation =
                azimuth.toFloat() * if (roll.absoluteValue > PI / 2) 1 else -1
            compassText.text = "${azimuth.roundToInt()}°"
        }
    }

    override fun onResume() {
        super.onResume()

        sensors.forEach {
            sensorManager.registerListener(this, it, SensorManager.SENSOR_DELAY_UI)
        }
    }

    override fun onPause() {
        super.onPause()

        sensorManager.unregisterListener(this)
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {}
}
