# SpinnyNeedle

## Git-repo
https://gitlab.com/tejp/spinnyneedle/

## Kortfattad beskrivning
Jag utgick från exemplet men skrev en egen version. Valde att använda Kotlin istället för jag har velat testa det. Utöver det man kunde få ut av exempelimplementationen hämtade jag resten från Android developer pages (API dokumentationen), Android källkoden, stackoverflow.com och Kotlin dokumentationen. De största förändringarna är hur sensorerna hanteras och de tilläggen jag valde att göra (visar på demot).

## APKn
Finns under releases i gitlab
